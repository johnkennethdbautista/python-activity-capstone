from abc import ABC, abstractclassmethod

class Person(ABC):
	@abstractclassmethod
	def getFullName(self):
		pass
	@abstractclassmethod
	def addRequest(self, request):
		pass
	@abstractclassmethod
	def checkRequest(self):
		pass
	@abstractclassmethod
	def addUser(self):
		pass
	@abstractclassmethod
	def login(self):
		pass
	@abstractclassmethod
	def logout(self):
		pass

class Employee(Person):
	def __init__(self, firstName, lastName, email, department):
		super().__init__()
		self._firstName = firstName;
		self._lastName = lastName;
		self._email = email;
		self._department = department;

	def setter_firstName(self, firstName):
		self._firstName = firstName;
	def setter_lastName(self, lastName):
		self._lastName = lastName;
	def setter_email(self, email):
		self._email = email;
	def setter_department(self, department):
		self._department = department;

	def getter_firstName(self, firstName):
		return self._firstName;
	def getter_lastName(self, lastName):
		return self._lastName;
	def getter_email(self, email):
		return self._email;
	def getter_department(self, department):
		return self._department;

	def getFullName(self):
		pass
	def addRequest(self, request):
		pass
	def checkRequest(self):
		pass
	def addUser(self):
		pass

	def login(self):
		print(f"{self._email} has logged in.");
	def logout(self):
		print(f"{self._email} has logged out.");

class TeamLead(Employee):
	def __init__(self, firstName, lastName, email, department):
		super().__init__(firstName, lastName, email, department);
		self._members = [];

	def setter_members(self, members):
		self._members += members;

	def getter_members(self):
		print(self._members)

	def getFullName(self):
		print(f"{self._firstName} {self._lastName}");

	def addRequest(self, request):
		pass;

	def checkRequest(self):
		print("Request Checked!");

	def addUser(self, user):
		pass;

	def login(self):
		pass;

	def logout(self):
		pass;

class Admin (Employee):
	def __init__ (self, firstName, lastName, email, department):
		super().__init__(firstName, lastName, email, department);

	def getFullName(self):
		print(f"{self._firstName} {self._lastName}");

	def addRequest(self,request):
		pass

	def checkRequest(self):
		pass        

	def addUser(self,user):
		print( f"User has been added"); 

	def login(self):
		pass;
	def logout(self):
		pass;


class Request ():
    def __init__ (self, name, requester, dateRequested, status):
        self._name = name;
        self._requester = requester;
        self._dateRequested = dateRequested;
        self._status = status;

    def updateRequest(self):
        print(f"Request {self._name} has been updated");

    def closeRequest(self):
        print(f"Request {self._name} has been closed");

    def cancelRequest(self):
        print(f"Request {self._name} has been cancel");

teamlead = TeamLead ("John", "Bautista", "jk@email.com", "IT Department");
admin = Admin ("JK", "Test", "jktest@email.com", "Dell Laptops");
request = Request ("SSO Inquiry", "John Smith", "09/01/2023", "pending for approval")

teamlead.getFullName();
admin.addUser("JK Bautista");
request.updateRequest();
